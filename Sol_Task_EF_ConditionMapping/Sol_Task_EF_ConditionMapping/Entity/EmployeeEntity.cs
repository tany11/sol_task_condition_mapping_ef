﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Task_EF_ConditionMapping.Entity
{
    class EmployeeEntity
    {
        public int? EmployeeId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public Boolean IsTerminate { get; set; }
    }
}
